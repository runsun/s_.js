//
// s_( s, data={}, deco=(x,i)=>x )
// 
// Javascript string substitution function 
// 
// By Runsun Pan (runsun at gmail dot com)
// MIT 2016 Runsun Pan
//

console.clear()

function s_( s, data={}, deco=(x,i)=>x )
{ 
  let i=-1, f 
  , typ= o=>{ 
    // https://toddmotto.com/understanding-javascript-types-and-reliable-type-checking/
       try{ let typ= Object.prototype.toString.call(o).slice(8, -1)
            typ = typ=='Number'?
                  ( parseInt(o,10)==o?'Integer':'float')
                  :typ
            return typ
            // Bolean, String, Array, Date, Error, Function,
            // Selection ( obj returned by getSelection() in FF)
            // Integer, Float, Regex
            // 
            // typ( Array ) => 'Function'
            // typ( [2,3] ) => 'Array'
            // typ( Myclass ) => 'Function'
            // typ( new MyClass ) => 'MyClass'
          }
          catch(e) {return false}
    }
  , isstr= x=> typeof(x)=='string'
  , isarr= x=> typ(x)=='Array'
  , isfunc= x=> typ(x)=='Function'
  
  if(isstr(data)){ data = [data] }
  
  let newdeco=deco, sColor='', sTag='', wrap=',' 
  
  if(!isfunc(deco))  // either string or array
  {  
    let ptn_color= /c:([#A-Za-z0-9,]{3,})/   // "c:red,green"
      , ptn_tag= /\/([A-Za-z]*)/             // "/b"
      , ptn_wrap= /.*,.*/                    // "a,b"
      , m
    let decos = isstr(deco)?[deco]:deco
    //console.log('decos=', decos)
    for(let j=0;j<decos.length;j++)
    { 
      //console.log('decos[j]=', decos[j])
    
      m= decos[j].match(ptn_color)
      //console.log('m= ', m)
      
      if(m){ m = m[1].split(',')
             let c  = m[0]?`color:${m[0]};`:''
             let bc = m[1]?`background:${m[1]}`:''
             sColor= ` style="${c+bc}"` 
             decos.splice(j,1)
             break 
           }
    }

    for(let j=0;j<decos.length;j++)
    { 
      m= decos[j].match(ptn_tag)
      if(m){ sTag=m[1]; decos.splice(j,1); break }
    }

    if(decos.length)
    {
      m= decos[0].match(ptn_wrap)
      //console.log('m = ', m)
      if(m) wrap = m[0]
    }
    //console.log(`wrap = "${wrap}"`)
    newdeco = x=>
    {
      sTag = sColor &&(!sTag)? 'span': sTag
      if(sTag || sColor)
        return `<${sTag}${sColor}>${wrap.replace(',',x)}</${sTag}>`
      else
        return wrap.replace(',',x)
        
    }
    
  }  
  
  if( isarr(data) ) // Array
  {  
    var rx= /\/\.\//g    
    f= ()=>{i++; 
            let ret= data[i]==undefined
                 ?'/./':newdeco(data[i],i)
						if(isarr(ret)){                  // To show ["x"] as '["x"]'
							try{ ret = JSON.stringify(ret)}  // but not just 'x'
							catch(e){}
						}			 
						return ret
		        }
  }
  else             // Object
  { 
    var rx = /\/\.[A-Z\.a-z0-9_]*(\=|:)?\.\//g
    f=(x)=>
    { i++;
      let key0 = x.slice(2,-2)
      let key  = key0.match(/(\=|:)$/)
                 ?key0.slice(0,-1):key0
      let label= key0.match(/:$/)
                 ?('<strong>'+key+'</strong>=')
                 :key0.match(/\=$/)?(key+"="):''
      let val
      if(key in data)
      { val = newdeco(data[key],i) }
      else
      {
         try{ val= eval(key) }
         catch(e){ val = x } 
      }  
			if(isarr(val)){                  // To show ["x"] as '["x"]'
				try{ val = JSON.stringify(val)}  // but not just 'x'
				catch(e){}
			}	
      return label+val 
    }  
  }
  return s.replace(rx,f)
}

s_.info ={ 
  name: 's_'
  ,  doc: ['']
  , tests: [ 
     ''
    ,'<b>String substitution function</b>'
    ,''
    ,'  s: string template'
    ,'  data: either array or object'
    ,'  deco: string, array or func(x,i)'
    ,''
    //------------------------------------------------
    ,'<hr/><b>A. data is an array:</b>'
    ,''
		,'// Use "/./" as a place holder in the template'
		,'// Note that in this case the data MUST be an array:'
    ,''
    ,`a> s = "color=/./, len=/./"  `
    ,`a> arr = ["blue", 3]`
    ,''
    ,`t> s_(s,arr) := color=blue, len=3`
    ,`t> s_(s,['blue']) := color=blue, len=/./`
    ,''
    ,'// If data is a string, it will be treated as [data]:'
    ,'' 
    ,`t> s_(s,'blue') := color=blue, len=/./`
    ,`t> s_("color=/./",['blue']) := color=blue`
    ,`t> s_("color=/./",[['blue']]) := color=["blue"]`
    ,''
    ,'// NOTE these (when data array contain array items):'
    ,''
    ,`t> s_(s,[['blue'],[3]]) := color=["blue"], len=[3]`
    ,`t> s_(s,[['blue','red'],[3,4]]) := color=["blue","red"], len=[3,4]`
    ,`t> s_(s,[''+['blue','red'],''+[3,4]]) := color=blue,red, len=3,4`
    ,''
    ,''
    //------------------------------------------------
    ,'<hr/><b>B1. data is an object:</b>'
    ,''
		,'// Use /.key./ as a place holder in template'
    ,''
    ,`a> obj= {color:"blue", len:3}`     
    ,`a> s2= "/.color./, /.len./"`
    ,`t> s_(s2, obj) := blue, 3`
    ,`t> s_(s2, {color: "blue"}) := blue, /.len./`
    ,`t> s_(s2, {len:3}) := /.color./, 3`
    ,`t> s_(s2, {color:['blue','red'], len:[3,4]}) := ["blue","red"], [3,4]`
    ,''
    ,'// This allows for repeated use of data:'
    ,`t> s_("/.color./, /.color./", {color:"red"}):= red, red`
    ,''
    ,''
    //------------------------------------------------
    ,'<hr/><b>B2. variable evaluation for object data:</b>'
    ,''
    ,`e> window.color = "red"`
    ,`t> s_(s2, {len:3}) := red, 3`
    ,''
    ,`e> window.len= 10`
    ,`t> s_(s2) := red, 10`
    ,''
    ,`e> window.aa = {color:"gray", len:5}`
    ,`t> s_("/.aa.color./, /.aa.len./") := gray, 5`
    ,''
    ,''
    //------------------------------------------------
    ,'<hr/><b>B3. Labeled output for object data: </b>'
    ,''
		,'// When adding a "=" after the key, '
    ,'// print out the key (as label) and its value:'
    ,''
    ,`a> s3= "/.color=./, /.len=./"`
    ,`t> s_(s3, obj) := color=blue, len=3`
    ,`t> s_(s3, {color:['blue','red'], len:[3,4]}) := color=["blue","red"], len=[3,4]`
    ,''
    ,'// When adding a ":" after the key, '
    ,'// the label is blod-faced:'
    ,''
    ,`a> s4= "/.color:./, /.len:./"`
    ,`t> s_(s4, obj) := <strong>color</strong>=blue, <strong>len</strong>=3`
    ,`t> s_("/.aa.color:./, /.aa.len:./") := <strong>aa.color</strong>=gray, <strong>aa.len</strong>=5`
    
    ,''
    ,''
    //------------------------------------------------
    ,'<hr/><b>D. Using the 3rd argument, <i>deco</i>:</b>'
    ,''
    ,'<i>deco</i> could be: "/x", "x,y", "c:x", "bc:x" or a function f(x)'
    ,''
    ,'// If deco="/x", wrap each value with &lt;x> and &lt;/x>'
    ,''
    ,`t> s_(s, arr, '/i') := color=<i>blue</i>, len=<i>3</i>` 
    ,`t> s_(s, arr, '/button') := color=<button>blue</button>, len=<button>3</button>` 
    ,`t> s_(s2, obj, '/b') := <b>blue</b>, <b>3</b>` 
    ,`t> s_(s3, obj, '/b') := color=<b>blue</b>, len=<b>3</b>` 
    
    ,''
    ,'// If deco="a,b", wrap each value with a and b'
    ,''
    ,`t> s_(s, arr, '@,#') := color=@blue#, len=@3#` 
    ,`t> s_(s2, obj, '+,-') := +blue-, +3-` 
    ,`t> s_(s3, obj, '[,]') := color=[blue], len=[3]` 
    //,`t> s_(s3, obj, '<a href="#">,</a>') := color=<a href="#">blue</a>, len=<a href="#">3</a>` 
    
    ,''
    ,'// If deco="c:some_color", paint the font with some_color:'
    ,''
    ,`t> s_(s, arr, 'c:red') := color=<span style="color:red;">blue</span>, len=<span style="color:red;">3</span>` 
    
    ,''
    ,'// deco="c:,some_color" to paint background color:'
    ,''
    ,`t> s_(s, arr, 'c:,#ccc') := color=<span style="background:#ccc">blue</span>, len=<span style="background:#ccc">3</span>` 
    
    ,''
    ,'// deco="c:color1,color2" to set font and background colors:'
    ,''
    ,`t> s_(s, arr, 'c:yellow,black') := color=<span style="color:yellow;background:black">blue</span>, len=<span style="color:yellow;background:black">3</span>` 
    
    //------------------------------------------------
    ,'<hr/>// When deco is a array of the above strings: '
    ,''
		,`t> s_(s3, obj, ['{,}','c:red,yellow','/i']) := color=<i style="color:red;background:yellow">{blue}</i>, len=<i style="color:red;background:yellow">{3}</i>` 
    

    ,''
    //------------------------------------------------
    ,'<hr/>// When deco is a function: '
    ,''
    ,`t> s_(s, arr, x=>"["+x+"]") := color=[blue], len=[3]`
    ,`t> s_(s2, obj, x=>"["+x+"]") := [blue], [3]`
    ,`t> s_(s3, obj, x=>"["+x+"]") := color=[blue], len=[3]`
    //,`t> s_(s2, obj, (x,i)=>"("+i+")"+x) := (0)blue, (1)3`
    ,''
    ,''
    //------------------------------------------------
    ,'<hr/>// More usages:'
    ,''
    ,'// Use the log_(s, data, deco)'
    ,'// This will output "aa.color=gray" to the console:'
    ,'// ==> so dont have to type it twice like: ("aa.color=", aa.color)'
    ,`e> log_("/.aa.color=./")`
    ,''
    ,'// Add some onclick events. This example will display'
    ,"// button's innerHTML to the console when clicked:"
    ,`a> al= x=>'<button onclick=\"console.log(this.innerHTML)\">'+x+'</button>'`
    ,`o> s_(s, arr, al) := color=[blue], len=[3]`
    
   ]
}     

function log_(s, data, deco)
{
   console.log( s_(s,data,deco) )
}  

function doctest() 
{
  // need: jxx and jQuery 
  // http://runsun.info/prog/js/proj/jxx/jxx.js

  function test() 
  {  
    testfuncs = [ s_
              ]
    var out
    out = testfuncs.map(x=> jx.test.func(x))
    document.body.innerHTML += out.join('')
  }  

  $.get('https://bitbucket.org/runsun/jxlog/raw/193083aae6fd268d0844e98cad8cec4137781c34/jxlog.js'
     , (data)=>
      {
        eval(data)
        window.log=jxlog
        test()
      } 
  )
}  

doctest()
